package aps2_pc27s_2s2017.Horarios;

import java.io.ObjectInputStream;
import java.net.Socket;

public class ClienteConsultaHorario {
    
    public static void main(String[] args) {
        
        new ClienteConsultaHorario().consultar();
    }
    
    public void consultar(){
        
        try {
              Socket cliente = new Socket("localhost",12345);
              ObjectInputStream entrada = new ObjectInputStream(cliente.getInputStream());
              Horario horario = (Horario)entrada.readObject();

              System.out.println("Data de abertura:" + horario.getHoraAbertura());
              System.out.println("Data de encerramento:" + horario.getHoraEncerramento());

              entrada.close();
              System.out.println("Conexão encerrada");
            }
            catch(Exception e) {
              System.out.println("Erro: " + e.getMessage());
        }
    }
}
