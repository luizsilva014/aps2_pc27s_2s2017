
package aps2_pc27s_2s2017.Horarios;

import java.io.Serializable;
import java.util.Date;

// Objeto que trafega pela rede, para que isso seja possívle implementa Serializable
// O servidor retorno o objeto Horario a uma requisição do cliente.
public class Horario implements Serializable{
    
    private String HoraAbertura;
    private String HoraEncerramento;

    public Horario(String HoraAbertura, String HoraEncerramento) {
        this.HoraAbertura = HoraAbertura;
        this.HoraEncerramento = HoraEncerramento;
    }
    
    public String getHoraAbertura() {
        return HoraAbertura;
    }

    public void setHoraAbertura(String HoraAbertura) {
        this.HoraAbertura = HoraAbertura;
    }

    public String getHoraEncerramento() {
        return HoraEncerramento;
    }

    public void setHoraEncerramento(String HoraEncerramento) {
        this.HoraEncerramento = HoraEncerramento;
    }
    
    
}
