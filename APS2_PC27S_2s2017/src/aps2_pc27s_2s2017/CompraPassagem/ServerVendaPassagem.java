package aps2_pc27s_2s2017.CompraPassagem;

import aps2_pc27s_2s2017.CompraPassagem.Interface.VendaPassagem;
import java.net.MalformedURLException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

public class ServerVendaPassagem extends UnicastRemoteObject implements VendaPassagem{

    public ServerVendaPassagem() throws RemoteException{
        super();
    }
   
    @Override
    public String vender(Compra compra) throws RemoteException {
        String retorno;
        //Valor da passagem é fixo em 100 reais
        
        if (compra.getValor() < 100) {
            return "O valor da passagem é de 100 reais.";
        }else if (compra.getValor() == 100) {
            retorno =  "Sua passagem foi comprada com sucesso!";
        }else{
            retorno =  "Sua passagem foi comprada com sucesso. E seu troco é de " + (compra.getValor() - 100);
        }

        System.out.println("Passagem vendia para: " + compra.getNome());
        System.out.println("Dia: " + compra.getDataPassagem());
        System.out.println("Troco: " + (compra.getValor() - 100));
        
        return retorno;
    }

    public static void main(String args[]) throws MalformedURLException{
        
        try{
            
            ServerVendaPassagem serv = new ServerVendaPassagem();
            // Registra nome do servidor
            Registry registry = LocateRegistry.createRegistry(8080);
            registry.rebind("ServidorVendaPasssagem", serv);
            System.out.println("Servidor remoto pronto.");
        }
        catch(RemoteException e){
            System.out.println("Exceção remota: " + e);
        }
    }
    
}
