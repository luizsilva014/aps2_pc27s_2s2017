package aps2_pc27s_2s2017.CompraPassagem;

import java.io.Serializable;

public class Compra implements Serializable{

    private String nome;
    private String dataPassagem;
    private Double valor;

    public Compra(String nome, String dataPassagem, Double valor) {
        this.nome = nome;
        this.dataPassagem = dataPassagem;
        this.valor = valor;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDataPassagem() {
        return dataPassagem;
    }

    public void setDataPassagem(String dataPassagem) {
        this.dataPassagem = dataPassagem;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }
}
