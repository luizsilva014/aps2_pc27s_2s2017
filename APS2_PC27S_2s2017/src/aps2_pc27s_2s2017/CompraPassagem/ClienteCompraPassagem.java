package aps2_pc27s_2s2017.CompraPassagem;

import aps2_pc27s_2s2017.CompraPassagem.Interface.VendaPassagem;
import java.rmi.Naming;

public class ClienteCompraPassagem {

    public static void main(String args[]){
        try{

            VendaPassagem serv = (VendaPassagem)Naming.lookup("//localhost:8080/ServidorVendaPasssagem");
            String retorno = serv.vender(new Compra("joão", "12/12/2017", 120.0));
            System.out.println(retorno);

        }
        catch(Exception e){}
    }

}
