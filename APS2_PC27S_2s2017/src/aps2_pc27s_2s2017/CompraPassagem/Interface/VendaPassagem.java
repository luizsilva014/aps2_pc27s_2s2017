package aps2_pc27s_2s2017.CompraPassagem.Interface;

import aps2_pc27s_2s2017.CompraPassagem.Compra;
import java.rmi.Remote;
import java.rmi.RemoteException;

public interface VendaPassagem extends Remote{
    
    public String vender(Compra compra) throws RemoteException;
    
}
