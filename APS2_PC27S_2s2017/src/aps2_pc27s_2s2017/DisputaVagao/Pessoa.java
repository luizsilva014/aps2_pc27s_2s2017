package aps2_pc27s_2s2017.DisputaVagao;

import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Pessoa implements Runnable{

    private String nome;
    private Vagao vagao;
    private final static Random generator = new Random();

    public Pessoa(String nome, Vagao vagao) {
        this.nome = nome;
        this.vagao = vagao;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    @Override
    public void run() {
        
        try {
            // Tempo até chegar no vagão
            Thread.sleep(generator.nextInt(1000));
        } catch (InterruptedException ex) {
            Logger.getLogger(Pessoa.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        if (this.vagao.entrar()) {
            System.out.println(this.nome + " entrou no " + this.vagao.getNome());
        }else{
            System.out.println(this.nome + " NÃO entrou no " + this.vagao.getNome() + ", vagão está lotado.");
        }    

    }
}
