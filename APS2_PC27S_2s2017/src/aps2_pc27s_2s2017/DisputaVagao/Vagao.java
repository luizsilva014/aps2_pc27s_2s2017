package aps2_pc27s_2s2017.DisputaVagao;

public class Vagao {
    
    private String nome;
    private int quantidadePessoas;

    public Vagao(String nome) {
        this.nome = nome;
        this.quantidadePessoas = 0;
    }
    
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getQuantidadePessoas() {
        return quantidadePessoas;
    }

    // O modificador de acesso synchronized não permite 
    // que duas pessoas entrem ao mesmo tempo no vagão
    public synchronized boolean entrar(){

        //Capacidade do vagão é de 50 pessoas
        if (this.quantidadePessoas <=50) {
            this.quantidadePessoas = this.quantidadePessoas + 1;
            return true;
        }
        
        return false;
    }

}
