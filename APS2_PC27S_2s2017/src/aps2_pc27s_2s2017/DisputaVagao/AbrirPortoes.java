
package aps2_pc27s_2s2017.DisputaVagao;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class AbrirPortoes {
    
    public static void main(String[] args) {

        Vagao vagao = new Vagao("Vagão 1");

        ExecutorService threadExecutor = Executors.newCachedThreadPool();

        // Simula a disputa entre 60 pessoas para entrar no vagão
        for (int i = 0; i < 60; i++) {
            threadExecutor.execute(new Pessoa("Pessoa " + (i+1), vagao));
        }

        threadExecutor.shutdown();
    }
    
}
